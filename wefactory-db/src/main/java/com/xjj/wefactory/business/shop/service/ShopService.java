/****************************************************
 * Description: Service for 商家
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-07-04 zhanghejie Create File
**************************************************/
package com.xjj.wefactory.business.shop.service;
import com.xjj.framework.service.XjjService;
import com.xjj.wefactory.business.shop.entity.ShopEntity;

public interface ShopService  extends XjjService<ShopEntity>{
	

}
