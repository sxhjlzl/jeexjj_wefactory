# jeexjj_wefactory
本项目做了一个项目重构，不再更新，新地址请移步
[https://gitee.com/jeexjj/jeexjj_wxmall](https://gitee.com/jeexjj/jeexjj_wxmall) 


#### 技术交流
- QQ群：174266358
- 微信群：加微信forever-evolution，把你拉群,备注：进群。
- ![](https://gitee.com/zhanghejie/jeexjj/raw/master/doc/img/jeexjj_wx.jpg)

#### JEEXJJ开源项目
 名称 | 地址
------|----
快速开发框架  | [https://gitee.com/zhanghejie/jeexjj](https://gitee.com/zhanghejie/jeexjj)
小程序商城  | [https://gitee.com/jeexjj/jeexjj_wxmall](https://gitee.com/jeexjj/jeexjj_wxmall)
VUE仿锤子商城  | [https://gitee.com/zhanghejie/jeexjj_mall](https://gitee.com/zhanghejie/jeexjj_mall)
防CAS单点登陆  | [https://gitee.com/zhanghejie/jeexjj_sso](https://gitee.com/zhanghejie/jeexjj_sso)
